# Framework PHP - Campus Ynov - Mars 2017

## Évaluation

L'évaluation porte sur la création d'une application selon des spécifications précises, énoncées ci-dessous. Cette
application sera testée de manière semi-automatisée, certains éléments pouvant nécessiter des vérifications manuelles.

## Modalités de rendu et de notation

L'application devra être rendue à 23h59 le 02/05/2017, sous la forme d'un fichier ZIP nommé selon la nomenclature
suivante : `votre_prénom-votre_nom.zip`, contenant tous les fichiers de l'application. Chaque jour de retard soustraira
2 points à votre note (10 jours de retard seront donc considérés comme un 0).

Un bonus pourra être appliqué en fonction de la qualité du code rendu, ainsi que sur les éléments indiqués comme bonus.

## Éléments fournis

- [Le cours](https://github.com/kblais/ynov-frameworks-php-slides)
- [L'application réalisée en cours](https://github.com/kblais/ynov-frameworks-php)
- Ce squelette d'application, contenant :
	- Laravel 5.4 initialisé
	- Système d'authentification pré-configuré
	- TwigBridge pré-configuré pour l'utilisation de Twig

## Spécifications

L'application servira à gérer des professeurs et la liste de leurs cours. Un utilisateur devra être connecté pour
accéder à toutes les informations de l'application.

Une base de données SQLite est utilisée pour le stockage des informations.

Des validations sur les données entrées dans les formulaires devront être réalisées en fonction du type d'information,
et les messages d'erreur devront être affichés et traduits en français.

### Professeurs

#### Colonnes

|          nom           |       utilité        |           type           |
|------------------------|----------------------|--------------------------|
| id                     | identifiant unique   | entier, auto-incrément   |
| first_name             | prénom               | chaine                   |
| last_name              | nom de famille       | chaine                   |
| email                  | email de contact     | chaine (email), unique   |
| independant_contractor | intervenant ?        | booléen                  |
| created_at             | date de création     | datetime                 |
| updated_at             | date de modification | datetime                 |
| deleted_at             | date de supression   | datetime, nul par défaut |

#### Routes web souhaitées

##### GET `/teacher`

Liste des professeurs paginée, avec pour chacun le nom, le prénom, et l'email.

##### GET `/teacher/create`

Formulaire de création d'un professeur.

##### POST `/teacher`

Création d'un professeur en base de données. Redirige vers la liste des professeurs.

##### GET `/teacher/{teacher}`

Vue d'un professeur, avec la liste de ses cours.

##### GET `/teacher/{teacher}/edit`

Formulaire de modification d'un professeur.

##### PUT/PATCH `/teacher/{teacher}`

Mise à jour du professeur en base de données. Redirige vers la liste des professeurs.

##### DELETE `/teacher/{teacher}`

Suppression (soft delete) du professeur. Redirige vers la liste des professeurs.

#### Routes API souhaitées

##### GET `/api/teacher`

Liste en JSON des professeurs, avec pagination.

##### GET `/api/teacher/{teacher}`

Informations sur un professeur en JSON, avec la liste de ses cours paginés.

### Cours

#### Colonnes

|     nom     |             utilité             |          type          |
|-------------|---------------------------------|------------------------|
| id          | identifiant unique              | entier, auto-incrément |
| title       | titre du cours                  | chaine                 |
| description | description du cours            | texte                  |
| teacher_id  | professeur du cours             | entier non signé       |
| starting_at | date et heure de début du cours | datetime               |
| ending_at   | date et heure de fin du cours   | datetime               |
| created_at  | date de création                | datetime               |
| updated_at  | date de mise à jour             | datetime               |

#### Routes web souhaitées

##### GET `/teacher/{teacher}/course/create`

Formulaire de création d'un cours pour le professeur concerné.

##### POST `/teacher/{teacher}/course/create`

Création d'un cours en base de données pour le professeur concerné. Redirige vers la vue professeur.

##### [BONUS] DELETE `/teacher/{teacher}/course/{course}`

Suppression du cours. Redirige vers la vue professeur.
