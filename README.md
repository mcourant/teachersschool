# Évaluation Framework PHP - Campus Ynov - Mars 2017

## Installation

- Copier le fichier `.env.example` en `.env`
- Créer un fichier `database/database.sqlite`
- Depuis la racine du projet, lancer `php artisan key:generate`
