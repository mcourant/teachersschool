<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Courses extends Model
{

  protected $fillable = [
    'title', 'description', 'teacher_id', 'starting_at', 'ending_at'
  ];

  protected $dates = ['starting_at', 'ending_at'];

  public function teacher()
  {
      return $this->belongsTo(Teacher::class);
  }

  public function setStartingAtAttribute($date)
    {
        $this->attributes['starting_at'] = Carbon::createFromFormat('d/m/Y h:i:s', $date)
            ->toDateTimeString();
    }

    public function setEndingAtAttribute($date)
      {
          $this->attributes['ending_at'] = Carbon::createFromFormat('d/m/Y h:i:s', $date)
              ->toDateTimeString();
      }

}
