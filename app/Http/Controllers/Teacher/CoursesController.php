<?php

namespace App\Http\Controllers\Teacher;

use App\Courses;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Courses\StoreRequest;
use App\Teacher;

class CoursesController extends Controller
{

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Teacher $teacher)
    {
        return view('courses.create', compact('teacher'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRequest $request, Teacher $teacher)
    {
      $course = new Courses($request->all());
      $course->teacher_id = $teacher->id;
      $course->save();

      $courses = $teacher->courses()
                    ->paginate();

      return view('teacher.show', compact("teacher", 'courses'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Courses  $courses
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teacher $teacher, Courses $course)
    {
        $course->delete();

        $courses = $teacher->courses()
                        ->paginate();

        return redirect()->route('teacher.show', compact('teacher', 'courses'));
    }
}
