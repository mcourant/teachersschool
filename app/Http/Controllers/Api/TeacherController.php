<?php

namespace App\Http\Controllers\Api;

use App\Teacher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $teachers = Teacher::paginate();

      return response()->json($teachers);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Teacher  $teacher
     * @return \Illuminate\Http\Response
     */
    public function show(Teacher $teacher)
    {
        $courses = $teacher->courses()
                          ->paginate();

        return response()->json(compact('teacher','courses'));
    }


}
