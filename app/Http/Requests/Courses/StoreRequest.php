<?php

namespace App\Http\Requests\Courses;

use Auth;
use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => ['required', 'min:15'],
            'description' => ['required', 'min:25'],
            'starting_at' => ['date', 'date_format:d/m/Y h:i:s'],
            'ending_at' => ['date', 'date_format:d/m/Y h:i:s']

        ];
    }
}
