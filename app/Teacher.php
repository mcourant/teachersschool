<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Teacher extends Model{

  use SoftDeletes;

  protected $fillable = [
    'last_name', 'first_name', 'email', 'independant_contractor'
  ];

  protected $perPage = 25;
  
  public function courses()
    {
        return $this->hasMany(Courses::class);
    }

}
